﻿using System;

namespace Example
{
    class Car
    {
        public void TurnOnEngine()
        {
            Console.WriteLine("The engine has been activated");
        }

        public void TurnOnCarAlarm()
        {
            Console.WriteLine("Car alarm activated");
        }
    }


}
